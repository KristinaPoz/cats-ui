import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';
import { errorResponse } from '../__mock__/error_resp';
/**
 * Класс для реализации логики рейтинга лайков и дизлайков 
 *
 */
export class RatingPage {
  private page: Page;
  private successRatingSelector = '.rating-names_table__Jr5Mf .has-text-success'
  private tableRating = 'Рейтинг имён котиков'
  public  errorSelector: string

  constructor({page}: {page: Page;}) {
    this.page = page;
    this.errorSelector = '//div[contains(text(), "Ошибка загрузки рейтинга")]';
}


async openRatingPage() {
    return await test.step('Открыть страницу рейтинга лайков и дизлайков ', async () => {
      await this.page.goto('/rating')
    })
}



getTitle() {
  return this.page.getByText(this.tableRating)
}

async getRating() {
  const ratingRaw = await this.page.locator(this.successRatingSelector).allTextContents()
  return ratingRaw.map( v => parseInt(v))
}

}

export type RatingPageFixture = TestFixture<
RatingPage,
	{
		page: Page;
	}
>;

export const ratingPageFixture: RatingPageFixture = async (
	{ page },
	use
) => {
	const raitingPage = new RatingPage({ page });

	await use(raitingPage);
};











