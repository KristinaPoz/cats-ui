import { test as base, expect } from '@playwright/test';
import { errorResponse } from '../__mock__/error_resp';
import { RatingPage, ratingPageFixture } from '../__page-object__';

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});


test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({ page, ratingPage,}) => {
  await page.route(
    request => request.href.includes('/api/likes/cats/rating'),
    async route => {
      await route.fulfill({
          ...errorResponse,
      });
    }
  );

  await ratingPage.openRatingPage();

  await expect(page.locator(ratingPage.errorSelector)).toBeVisible();
});

  
test('Рейтинг рейтинг количества лайков отображается по убыванию', async ({ page, ratingPage }) => {
  await ratingPage.openRatingPage()

  await expect(ratingPage.getTitle()).toBeVisible()

  let Listrating = await ratingPage.getRating()

  let orderLikes = Listrating.slice()

  orderLikes.sort((a, b) => b - a)

  expect(Listrating).toEqual(orderLikes)

});
